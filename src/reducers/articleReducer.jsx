import { actionType } from '../actions/actionType'
const initState = {
    articles: [],
    article: [],

}
export const articleReducer = (state = initState, action) => {
    switch (action.type) {
        case actionType.FETCH_ARTICLE:
            return { ...state, articles: action.payload }
        case actionType.DELETE_ARTICLE:
            return { ...state, articles: action.payload }
        case actionType.GET_ARTICLE_BY_ID:
            return { ...state, article: action.payload}
        case actionType.ADD_ARTICLE:
            return { ...state ,isAdd:action.isAdd}
            case actionType.UPDATE_ARTICLE_BY_ID:
                return {...state}
        default:
            return state;
    }
}