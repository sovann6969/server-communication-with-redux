import React, { Component } from 'react'
import axios from 'axios'
import { Link, useParams } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux'
import { getArticleById } from '../actions/acticleAction'
class ViewDetail extends Component {
    constructor(props) {
        super(props)

        this.state = {
            title: '',
            dec: '',
        }
    }

    componentDidMount() {
        let st = window.location.href
        let url = st.split("/")
        let id = url[4]

        this.props.getArticleById(id)
    }
    render() {
        console.log(this.props.article)
        return (
            <div className="col-12 col-sm-12 col-md-12 d-flex justify-content-center py-5">
                <div className="card" >
                    <div className="card-body">
                        <h5 className="card-title">Title : {this.props.article.TITLE}</h5>
                        <p className="card-text"> <span className="font-weight-bold">Description</span>  : {this.props.article.DESCRIPTION}</p>

                        <Link to="/admin" className="btn btn-primary btn-sm card-link ">Back to System</Link>
                    </div>
                </div>
            </div>


        )
    }
}
const mst = (store) => {
    return {
        article: store.articleReducer.article
    }
}

export default connect(mst, { getArticleById })(ViewDetail)