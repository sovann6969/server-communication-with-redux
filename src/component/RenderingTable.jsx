
import React, { Component } from 'react'
import '../route/App.css';
import axios from 'axios'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Swal from 'sweetalert2'
import { connect } from 'react-redux'
import { fetchArticle, deleteArticle, getArticleById } from '../actions/acticleAction'
class RenderingTable extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isDel: false
        }
    }
    componentDidMount() {
        this.props.fetchArticle()
        if (this.props.isDel) {
            this.setState({
                isDel: this.props.isDel,
            })
        }
    }

    notify = () => toast("Record has been deleted", {
        autoClose: 1500, position: toast.POSITION.BOTTOM_RIGHT
        , closeButton: false, pauseOnFocusLoss: true, draggablePercent: 60, type: toast.TYPE.SUCCESS
    });
    comfirmDel = (id) => {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
        }).then((result) => {
            if (result.value) {
                this.props.deleteArticle(id)
                // this.props.fetchArticle()
                this.notify()
            }
        })
    }
    comfirmUpdate = (id) => {
        //get data 
        this.props.getArticleById(id)
        this.props.handlerUpdate(id)
    }
    render() {
        return (
            <div className="col-md-12 col-sm-12 col-12 p-0">
                <h3 className="py-2 font-italic font-weight-light text-center">សូមស្វាគមន៍មកកាន់់ទំព័រប្រតិបត្តិការរបស់វេបសាយ</h3>

                <div className="d-flex justify-content-center py-5 ">


                    <button className="btn btn-lg btn-outline-primary " onClick={() => this.props.handlerAdd()}>Add</button>
                </div>
                <table className="table table-dark">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th >Created Date</th>
                            <th  >Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.articles.map((article, index) =>
                            <tr key={index}>
                                <td>{article.ID}</td>
                                <td>{article.TITLE}</td>
                                <td>{article.DESCRIPTION}</td>
                                <td>{article.CREATED_DATE}</td>
                                <td>
                                    <button className="btn-sm btn-primary m-1" onClick={() => this.comfirmUpdate(article.ID)}>Update</button>
                                    <button className="btn-sm btn-info m-1" onClick={() => this.props.handlerView(article.ID)}>View</button>
                                    <button className="btn-sm btn-danger m-1" onClick={() => this.comfirmDel(article.ID)}>Delete</button>

                                    <ToastContainer />
                                </td>
                            </tr>
                        )};
    </tbody>
                </table>
            </div>
        )
    }

}
const mapStateToProp = (store) => {
    return {
        articles: store.articleReducer.articles
    }
}
export default connect(
    mapStateToProp,
    { fetchArticle, deleteArticle, getArticleById })(RenderingTable)

