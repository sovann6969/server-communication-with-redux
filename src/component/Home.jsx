import React, { Component } from 'react'
import Img1 from '../img/1.jpg'
import 'bootstrap/dist/css/bootstrap.min.css';
export default class Home extends Component {
    render() {
        return (
            // <div style={{background: `url(${Img1}) no-repeat center`,backgroundSize: "contain" ,height:'700px'}}>
            //     <h3 className="py-5 font-italic font-weight-light text-center">សូមស្វាគមន៍មកកាន់់ទំព័រដើមរបស់វេបសាយ</h3>

            // </div>
            <div>

                <div className="py-5 bg-image-full object" style={{ backgroundImage: `url('https://unsplash.it/1900/1080?image=1076')`,width:'100%',height:'600px' }}>
                </div>


                <div className="py-5">
                    <div className="container">
                        <h1>Welcome to our home page</h1>
                        <p className="lead">This page will show you nothing </p>
                        <p>The article Api project (server communication) is in the system </p>
                    </div>
                </div>


                <div className="py-5 bg-image-full" style={{ backgroundImage: `url('https://unsplash.it/1900/1080?image=1081')` }}>

                    <div style={{ height: '500px' }}></div>
                </div>


                <div className="py-5">
                    <div className="container">
                        <h1>Click other page to see the content</h1>
                        <p className="lead">Mini project redux-pattern 5 pages</p>
                        <p>Deadline is on 12th november 2019</p>
                    </div>
                </div>


                <div className="py-5 bg-dark">
                    <div className="container">
                        <p className="m-0 text-center text-white">Copyright &copy; Your Website 2019</p>
                    </div>

                </div>

            </div>
        )
    }
}
