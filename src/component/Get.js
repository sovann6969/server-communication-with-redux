import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
import RenderingTable from './RenderingTable'
import CusForm from './CusForm';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { connect } from 'react-redux'
import { getArticleById } from '../actions/acticleAction'
export default class Get extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isAdd: false,
      isUpdate: false,
      isView: false,
      isDel: false,
      id: '',
      article: [],
    }
  }


  handlerAdd = () => {
    this.setState({
      isAdd: true,
      isUpdate: false,
      isView: false,
      isDel: false,
    })
  };
  handlerDel = () => {
    this.setState({
      isAdd: false,
      isUpdate: false,
      isView: false,
      isDel: true,
    })
  }
  handlerUpdate = articleId => {
    axios.get(`http://www.api-ams.me/v1/api/articles/` + articleId)
      .then(res => {
        this.setState({
          isAdd: false,
          isUpdate: true,
          isView: false,
          isDel: false,
          article: res.data.DATA
        })
      }).catch(e => console.log(e))



  }
  handlerView = articleId => {
    this.setState({
      isAdd: false,
      isUpdate: false,
      isView: true,
      isDel: false,
      id: articleId
    })
  }
  handlerReredner = () => {
    this.setState({
      isAdd: false,
      isUpdate: false,
      isView: false,
      isDel: false,
    })
  };

  onSubmitData = (check) => {
    if (check == "isAdd") {
      this.setState({
        isAdd: false,
        isUpdate: false,
        isView: false,
      })
      this.notif("Record has been Added Successfully");
    } else if (check == "isUpdate") {
      this.setState({
        isAdd: false,
        isUpdate: false,
        isView: false,
      })
      this.notif("Record has been updated Successfully  !");

    }

  }
  notif = (msg) => {
    toast(msg, {
      autoClose: 1500, pauseOnFocusLoss: false,
      position: toast.POSITION.TOP_RIGHT, className: 'rotateY animated', closeButton: false, draggablePercent: 60, type: toast.TYPE.SUCCESS
    })
  }

  render() {
    return (
      <div>
        {this.state.isView ? <Redirect to={{ pathname: "/view/" + this.state.id, state: { id: this.state.id } }} /> :
          (this.state.isUpdate ? <CusForm handlerReredner={this.handlerReredner} isUpdate={this.state.isUpdate} article={this.state.article} onSubmitData={this.onSubmitData} /> :
            (this.state.isAdd ? <CusForm handlerReredner={this.handlerReredner} isAdd={this.state.isAdd} onSubmitData={this.onSubmitData} /> : (this.state.isDel ?
              <RenderingTable isDel={"true"} handlerAdd={this.handlerAdd}
                handlerView={this.handlerView} handlerDel={this.handlerDel} handlerUpdate={this.handlerUpdate} /> : <RenderingTable isDel={"false"} handlerAdd={this.handlerAdd}
                  handlerView={this.handlerView} handlerDel={this.handlerDel} handlerUpdate={this.handlerUpdate} />)))
        }
      </div>
    )
  }
}


