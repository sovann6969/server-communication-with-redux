import axios from 'axios'
import { actionType } from './actionType'

export const fetchArticle = () => {
    return (dispatch) => {
        axios.get('http://api-ams.me/v1/api/articles?page=1&limit=15')
            .then(result => {
                // console.log("result", result);
                dispatch({
                    type: actionType.FETCH_ARTICLE,
                    payload: result.data.DATA
                })
            })
            .catch(error => console.log("eroror fetch", error))
    }
}
export const deleteArticle = (id) => {
    return (dispatch) => {
        axios.delete('http://www.api-ams.me/v1/api/articles/' + id).then(res => {
            axios.get('http://api-ams.me/v1/api/articles?page=1&limit=15')
                .then(result => {
                    dispatch({
                        type: actionType.FETCH_ARTICLE,
                        payload: result.data.DATA
                    })
                })
                .catch(error => console.log("eroror fetch", error))
        })
    }
}
export const getArticleById = (id) => {
    return (dispatch) => {
        axios.get(`http://www.api-ams.me/v1/api/articles/` + id)
            .then(res => {
                dispatch({
                    type: actionType.GET_ARTICLE_BY_ID,
                    payload: res.data.DATA
                })
            }).catch(e => console.log(e))
    }
}

export const addArticle = (article) => {
    return (dispatch) => {
        axios('http://www.api-ams.me/v1/api/articles', {
            method: 'POST',
            data: { "TITLE": article.title, "DESCRIPTION": article.description }
        }).then(res =>
            dispatch({
                type: actionType.ADD_ARTICLE,
                isAdd: false
            })
        ).catch(e => console.log(e))
    }
}

export const updateArticleById = (id,article) => {
    return (dispatch) => {
        axios('http://www.api-ams.me/v1/api/articles/' + id, {
            method: 'PUT',
            data: { "TITLE": article.title, "DESCRIPTION": article.description }
        }).then(res => 
            dispatch({
                    type:actionType.UPDATE_ARTICLE_BY_ID,
            })).catch(e => console.log(e))
    }
}